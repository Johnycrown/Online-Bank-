package com.example.digicorbankingapplication.data.model;

import lombok.Data;

@Data
public enum TransactionType {
    WITHDRAWAL, Deposit;
}
